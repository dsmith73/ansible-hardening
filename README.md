Ansible Hardening
=========

Hardening role with some sane hardening rules for RHEL/CentOS/Fedora.

Requirements
------------

None.

Role Variables
--------------

A list of firewalld services

    firewalld_services:
      - ssh

Architecture type. Either b32 or b64.

    arch_type: b64

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: all
      roles:
         - role: ansible-hardening 
           arch_type: b64 
           firewalld_services:
             - ssh

License
-------

BSD

Author Information
------------------

John Hooks
